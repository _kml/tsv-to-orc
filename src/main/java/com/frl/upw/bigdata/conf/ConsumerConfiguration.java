package com.frl.upw.bigdata.conf;

import lombok.Getter;
import lombok.Setter;

public class ConsumerConfiguration {

  @Getter
  @Setter
  private int consumerThreadNumber;

  @Getter
  @Setter
  private int workerThreadNumber;

  @Getter
  @Setter
  private String cacheFolder;

  @Getter
  @Setter
  private int timeSliceMinutes;

  @Getter
  @Setter
  private String orcTemplatePath;

  @Getter
  @Setter
  private String pidLocation;

  @Getter
  @Setter
  private Hdfs hdfs;

  @Getter
  @Setter
  private Kafka kafka;

  public class Hdfs {
    @Getter
    @Setter
    private String hdfsSite;

    @Getter
    @Setter
    private String coreSite;

    @Getter
    @Setter
    private String baseDir;
  }

  public class Kafka {
    @Getter
    @Setter
    private String consumerGroup;

    @Getter
    @Setter
    private String bootstrapServers;

    @Getter
    @Setter
    private String topicName;
  }
}
