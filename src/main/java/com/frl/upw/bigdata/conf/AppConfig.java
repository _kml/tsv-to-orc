package com.frl.upw.bigdata.conf;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class AppConfig {

  private static AppConfig instance = new AppConfig();

  private static ConsumerConfiguration config;

  private AppConfig() {}

  public static void initWithFile(String filePath) {
    ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
    try {
      config = mapper.readValue(new File(filePath), ConsumerConfiguration.class);
    } catch (Exception exp) {
      log.error("error creating configuration", exp);
    }
  }

  public static AppConfig getAppConfig() {
    return instance;
  }

  public ConsumerConfiguration getConsumerConfig() {
    return config;
  }
}
