package com.frl.upw.bigdata.threads;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.BlockingQueue;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;

import com.frl.upw.bigdata.conf.AppConfig;
import com.frl.upw.bigdata.conf.ConsumerConfiguration;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class MoveToHDFSThread implements Runnable {
  private BlockingQueue<Path> pathQueue;
  private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd/HH/mm/");

  public MoveToHDFSThread(BlockingQueue<Path> pathQueue) {
    this.pathQueue = pathQueue;
  }

  @Override
  public void run() {
    try {

      Configuration hdfsConf = new Configuration();
      hdfsConf.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
      hdfsConf.set("fs.file.impl", LocalFileSystem.class.getName());

      ConsumerConfiguration consumerConfig = AppConfig.getAppConfig().getConsumerConfig();
      hdfsConf.addResource(new Path(consumerConfig.getHdfs().getCoreSite()));
      hdfsConf.addResource(new Path(consumerConfig.getHdfs().getHdfsSite()));

      final FileSystem fs = FileSystem.get(hdfsConf);

      String hdfsBaseDir = consumerConfig.getHdfs().getBaseDir();
      hdfsBaseDir = hdfsBaseDir.endsWith("/") ? hdfsBaseDir : hdfsBaseDir + "/";

      int timeSlice = consumerConfig.getTimeSliceMinutes();

      while (true) {
        try {
          Path path = pathQueue.take();
          log.debug("copying " + path);
          log.debug("removing crc file before copying");
          try {
            Files.delete(
                Paths.get(path.toString().replace(path.getName(), "." + path.getName() + ".crc")));
          } catch (NoSuchFileException exp) {
            log.warn("crc file not found", exp.getMessage());
          }
          fs.moveFromLocalFile(path, createFullPath(path, hdfsBaseDir, timeSlice));
        } catch (IOException exp) {
          log.error(exp);
        }
      }

    } catch (IllegalArgumentException | IOException exp) {
      log.error("couldn't start MoveToHDFSThread, files would be only saved to cache folder", exp);
    } catch (InterruptedException intexp) {
      log.error("Thread interrupted", intexp.getMessage());
    } catch (Exception exp) {
      log.error(exp);
    } finally {
      log.debug("exiting MoveToHDFSThread");
    }

  }

  private Path createFullPath(Path localPath, String hdfsBaseDir, int timeSlice) {

    long fileEpochTime = Long.valueOf(localPath.getName().split("-")[0]);
    LocalDateTime dateTime =
        LocalDateTime.ofInstant(Instant.ofEpochSecond(fileEpochTime), ZoneId.systemDefault());

    LocalDateTime startingDT = getStartingDT(dateTime, timeSlice);

    return new Path(hdfsBaseDir + startingDT.format(formatter) + localPath.getName());
  }

  public int getStartingMinutes(LocalDateTime datetime, int timeSlice) {
    return ((datetime.getHour() * 60 + datetime.getMinute()) / timeSlice) * timeSlice;
  }

  private LocalDateTime getStartingDT(LocalDateTime datetime, int timeSlice) {
    int startingMinutes = getStartingMinutes(datetime, timeSlice);
    return datetime.minusMinutes(datetime.getHour() * 60 + datetime.getMinute() - startingMinutes);
  }
}
