package com.frl.upw.bigdata.threads;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;

import org.apache.hadoop.fs.Path;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;

import com.frl.upw.bigdata.OrcWriter;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class ConsumerThread implements Runnable {
  private String topic;
  private Properties props;
  private BlockingQueue<Path> pathQueue;
  private KafkaConsumer<String, String> consumer;

  public ConsumerThread(String topic, Properties props, BlockingQueue<Path> pathQueue) {
    this.topic = topic;
    this.props = props;
    this.pathQueue = pathQueue;
  }

  @Override
  public void run() {
    consumer = new KafkaConsumer<>(props);
    consumer.subscribe(Collections.singletonList(topic));

    ConsumerRecord<String, String> currentRecord = null;
    TopicPartition currentPartition = null;
    boolean hasError = false;

    try {
      while (!hasError) {
        ConsumerRecords<String, String> records = consumer.poll(Long.MAX_VALUE);
        for (TopicPartition partition : records.partitions()) {
          List<ConsumerRecord<String, String>> partitionRecords = records.records(partition);
          for (ConsumerRecord<String, String> record : partitionRecords) {
            boolean recordWritten = false;
            int retryCount = 0;
            while (!recordWritten && retryCount < 5) {
              try {
                OrcWriter.getInstance().write(record.value(), pathQueue);
                recordWritten = true;
              } catch (IOException ioexp) {
                retryCount++;
                log.error("couldn't write " + record.value() + " to orc, retrying " + retryCount);
              }
            }

            if (!recordWritten) {
              try {
                OrcWriter.getInstance().write(record.value(), pathQueue);
                log.debug("writing " + record.offset() + "-" + record.partition());
              } catch (IOException ioexp) {
                hasError = true;
                log.error("couldn't write " + record.value()
                    + " to orc, closing consumer after 5 retries", ioexp);
              }
            }
            currentRecord = record;
            currentPartition = partition;
          }

          long lastOffset = currentRecord.offset();
          consumer.commitSync(
              Collections.singletonMap(partition, new OffsetAndMetadata(lastOffset + 1)));
          currentRecord = null;
          currentPartition = null;
        }
      }

    } catch (WakeupException e) {
      // ignore for shutdown
    } catch (Exception exp) {
      log.error(exp);
    }

    finally {
      if (currentRecord != null) {
        long lastOffset = currentRecord.offset();
        log.debug("commiting " + lastOffset + "-" + currentPartition.partition());
        consumer.commitSync(
            Collections.singletonMap(currentPartition, new OffsetAndMetadata(lastOffset + 1)));
        log.debug(lastOffset + "-" + currentPartition.partition() + " commited, exiting consumer");
      }
      try {
        consumer.close();
      } catch (Exception exp) {

      }
      log.debug("consumer closed");
    }
  }

  public void shutdown() {
    consumer.wakeup();
  }
}
