package com.frl.upw.bigdata.orc;

import org.apache.orc.TypeDescription;
import org.apache.orc.TypeDescription.Category;

import lombok.Getter;
import lombok.Setter;

public class OrcField {

  @Getter
  @Setter
  private String name;

  @Getter
  @Setter
  private String type;

  @Getter
  @Setter
  private int index;

  public OrcField() {}

  public OrcField(String name, String type, int index) {
    this.name = name;
    this.type = type;
    this.index = index;
  }

  public OrcField(OrcField that) {
    this.name = that.name;
    this.type = that.type;
    this.index = that.index;
  }
  
  public String toString() {
    return name + ":" + type;
  }

  public Category getCategory() {
    return Category.valueOf(type.toUpperCase());
  }

  public TypeDescription getTypeDescription() {
    TypeDescription result = null;
    switch (getCategory()) {
      case BOOLEAN:
        result = TypeDescription.createBoolean();
        break;
      case BYTE:
        result = TypeDescription.createByte();
        break;
      case SHORT:
        result = TypeDescription.createShort();
        break;
      case INT:
        result = TypeDescription.createInt();
        break;
      case LONG:
        result = TypeDescription.createLong();
        break;
      case FLOAT:
        result = TypeDescription.createFloat();
        break;
      case DOUBLE:
        result = TypeDescription.createDouble();
        break;
      case STRING:
        result = TypeDescription.createString();
        break;
      case DATE:
        result = TypeDescription.createDate();
        break;
      case TIMESTAMP:
        result = TypeDescription.createTimestamp();
        break;
      case BINARY:
        result = TypeDescription.createBinary();
        break;
      case DECIMAL:
        result = TypeDescription.createDecimal();
        break;
      case VARCHAR:
        result = TypeDescription.createVarchar();
        break;
      case CHAR:
        result = TypeDescription.createChar();
        break;
      default:
        break;
    }
    return result;
  }
}
