package com.frl.upw.bigdata.orc;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hive.ql.exec.vector.BytesColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.ColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.LongColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.TimestampColumnVector;
import org.apache.hadoop.hive.ql.exec.vector.VectorizedRowBatch;
import org.apache.orc.TypeDescription;

import lombok.Getter;

public class OrcSchema {

  @Getter
  private List<OrcField> fields;

  private TypeDescription struct;

  public OrcSchema() {
    fields = new ArrayList<>();
    struct = TypeDescription.createStruct();
  }

  public OrcSchema(OrcSchema that) {
    fields = new ArrayList<>();
    struct = TypeDescription.createStruct();
    for (OrcField thatField : that.fields) {
      fields.add(new OrcField(thatField));
      struct.addField(thatField.getName(), thatField.getTypeDescription());
    }
  }

  public void addField(OrcField field) {
    fields.add(field);
    struct.addField(field.getName(), field.getTypeDescription());
  }

  public TypeDescription getTypeDescription() {
    return struct;
  }

  public VectorizedRowBatch addRowToBatch(VectorizedRowBatch batch, String[] content) {

    int row = batch.size++;
    for (int index = 0; index < fields.size(); index++) {
      OrcField field = fields.get(index);
      switch (field.getCategory()) {
        case INT:
        case BOOLEAN:
          fillLongColumneVector(batch.cols[index], row, content[field.getIndex()]);
          break;
        case STRING:
        case BINARY:
        case CHAR:
        case VARCHAR:
          fillBytesColumnVector(batch.cols[index], row, content[field.getIndex()]);
          break;
        case TIMESTAMP:
          fillTimestampColumnVector(batch.cols[index], row, content[field.getIndex()]);
        default:
          break;
      }
    }

    return batch;
  }

  private void fillLongColumneVector(ColumnVector columnVector, int row, String columnContent) {
    ((LongColumnVector) columnVector).vector[row] = Long.valueOf(columnContent);
  }

  private void fillBytesColumnVector(ColumnVector columnVector, int row, String columnContent) {
    ((BytesColumnVector) columnVector).setVal(row, columnContent.getBytes());
  }

  private void fillTimestampColumnVector(ColumnVector columnVector, int row, String columnContent) {
    ((TimestampColumnVector) columnVector).set(row, new Timestamp(Long.valueOf(columnContent)));
  }
}
