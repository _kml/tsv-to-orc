package com.frl.upw.bigdata.orc;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class SchemaFinder {

  public static OrcSchema findOrcSchemaFromJsonTemplate(String jsonTemplateFilePath)
      throws FileNotFoundException {
    
    Gson gson = new Gson();
    Reader reader = new FileReader(jsonTemplateFilePath);
    JsonObject jsonObj = gson.fromJson(reader, JsonElement.class).getAsJsonObject();

    OrcSchema schema = new OrcSchema();
    Set<Entry<String, JsonElement>> entrySet = jsonObj.entrySet();
    for (Map.Entry<String, JsonElement> entry : entrySet) {
      OrcField field = gson.fromJson(entry.getValue(), OrcField.class);
      field.setIndex(Integer.valueOf(entry.getKey()));
      schema.addField(field);
    }

    return schema;
  }
}
