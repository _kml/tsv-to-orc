package com.frl.upw.bigdata;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.hadoop.fs.Path;
import org.apache.log4j.LogManager;

import com.frl.upw.bigdata.conf.AppConfig;
import com.frl.upw.bigdata.conf.ConsumerConfiguration;
import com.frl.upw.bigdata.threads.ConsumerThread;
import com.frl.upw.bigdata.threads.MoveToHDFSThread;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class TsvKafkaConsumer {

  public static void main(String[] args) {

    log.debug("starting ..........");
    if (args.length < 1) {
      System.err.println("please provide config file");
      log.error("please provide config file");
      System.exit(2);
    }

    AppConfig.initWithFile(args[0]);
    ConsumerConfiguration consumerConfig = AppConfig.getAppConfig().getConsumerConfig();

    // get pid
    String jvmName = ManagementFactory.getRuntimeMXBean().getName();
    long pid = Long.valueOf(jvmName.split("@")[0]);

    // write pid to file
    String pidDir = consumerConfig.getPidLocation();
    pidDir = pidDir.endsWith("/") ? pidDir : pidDir + "/";
    String pidFile = pidDir + "tsv-to-orc.pid";
    try {
      Files.write(Paths.get(pidFile), String.valueOf(pid).getBytes());
    } catch (IOException exp) {
      log.error(exp);
    }

    // scan cache folder for old orc files
    log.debug("scanning for old orc files");
    List<Path> oldFiles = new ArrayList<Path>();
    try {
      oldFiles = Files.list(Paths.get(consumerConfig.getCacheFolder())) //
          .filter(p -> p.getFileName().toString().endsWith(".orc")) //
          .map(p -> new Path(p.toUri())) //
          .collect(Collectors.toList());
    } catch (IOException ioexp) {
      log.warn(ioexp);
    }
    if (oldFiles.isEmpty())
      log.debug("no old orc file found");
    else
      oldFiles.stream().forEach(f -> log.debug("old file found: " + f.toString()));

    // kafka consumer properties
    Properties consumerProps = new Properties();
    consumerProps.put("bootstrap.servers", consumerConfig.getKafka().getBootstrapServers());
    consumerProps.put("group.id", consumerConfig.getKafka().getConsumerGroup());
    consumerProps.put("auto.offset.reset", "earliest");
    consumerProps.put("enable.auto.commit", "false");
    consumerProps.put("key.deserializer",
        "org.apache.kafka.common.serialization.StringDeserializer");
    consumerProps.put("value.deserializer",
        "org.apache.kafka.common.serialization.StringDeserializer");

    int consumerThreadsCount = Integer.valueOf(consumerConfig.getConsumerThreadNumber());

    ThreadPoolExecutor consumerThreadsExecutor =
        (ThreadPoolExecutor) Executors.newFixedThreadPool(consumerThreadsCount);

    List<ConsumerThread> consumerThreads = new ArrayList<>();

    String topicName = consumerConfig.getKafka().getTopicName();

    BlockingQueue<Path> pathQueue = new LinkedBlockingQueue<>();

    for (int i = 0; i < consumerThreadsCount; i++) {
      ConsumerThread consumerThread = new ConsumerThread(topicName, consumerProps, pathQueue);
      consumerThreads.add(consumerThread);
      consumerThreadsExecutor.submit(consumerThread);
    }

    Thread copyThread = new Thread(new MoveToHDFSThread(pathQueue));
    copyThread.start();

    // add old orc files to pathQueue to copy them first
    pathQueue.addAll(oldFiles);

    Runtime.getRuntime().addShutdownHook(new Thread("shutdown thread") {
      public void run() {
        log.debug("shutting down");
        for (ConsumerThread consumer : consumerThreads) {
          consumer.shutdown();
        }

        OrcWriter.close();
        copyThread.interrupt();

        consumerThreadsExecutor.shutdown();
        try {
          consumerThreadsExecutor.awaitTermination(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException exp) {
          consumerThreadsExecutor.shutdownNow();
          log.error(exp);
        }
        try {
          Files.deleteIfExists(Paths.get(pidFile));
        } catch (IOException exp) {
          log.warn(exp);
        }
        log.debug("shutdown finished");
        LogManager.shutdown();
      }
    });
  }
}
