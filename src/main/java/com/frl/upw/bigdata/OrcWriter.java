package com.frl.upw.bigdata;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.hive.ql.exec.vector.VectorizedRowBatch;
import org.apache.orc.OrcFile;
import org.apache.orc.Writer;

import com.frl.upw.bigdata.conf.AppConfig;
import com.frl.upw.bigdata.conf.ConsumerConfiguration;
import com.frl.upw.bigdata.orc.OrcSchema;
import com.frl.upw.bigdata.orc.SchemaFinder;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class OrcWriter {

  private static OrcWriter instance;

  private Writer writer;
  private LocalDateTime startingTime;
  private Configuration localConf;
  private OrcSchema schema;
  private ConsumerConfiguration consumerConfig;
  private VectorizedRowBatch batch;
  private Path localPath;

  private static Object writerLock = new Object();

  private OrcWriter() {
    consumerConfig = AppConfig.getAppConfig().getConsumerConfig();

    localConf = new Configuration();
    localConf.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
    localConf.set("fs.file.impl", LocalFileSystem.class.getName());
    
    try {
      schema = SchemaFinder.findOrcSchemaFromJsonTemplate(consumerConfig.getOrcTemplatePath());
      batch = schema.getTypeDescription().createRowBatch();
    } catch (FileNotFoundException exp) {
      log.error("error creating schema", exp);
      System.exit(3);
    }
    LocalDateTime now = LocalDateTime.now(ZoneId.systemDefault());
    startingTime = getCurrentStartingDT(now);
    try {
      createWriter(now);
    } catch (IOException ioexp) {
      log.error("error initializing OrcWriter", ioexp);
    }
  }

  public static OrcWriter getInstance() {
    if (instance == null) {
      synchronized (writerLock) {
        if (instance == null) {
          instance = new OrcWriter();
        }
      }
    }
    return instance;
  }

  public static void close() {
    if (instance != null)
      instance.closeInternal();
  }

  private void closeInternal() {
    if (writer != null) {
      try {
        log.debug("adding message in batch to writer before closing");
        writer.addRowBatch(batch);
        log.debug("closing orc writer");
        writer.close();
        log.debug("orc writer closed");
        writer = null;
      } catch (IOException ioexp) {
        log.error("error closing orc writer", ioexp);
      }
    }
  }

  public void write(String record, BlockingQueue<Path> pathQueue) throws IOException {

    LocalDateTime now = LocalDateTime.now();

    if (startingTime.plusMinutes(consumerConfig.getTimeSliceMinutes()).isBefore(now)) {
      synchronized (writerLock) {
        if (startingTime.plusMinutes(consumerConfig.getTimeSliceMinutes()).isBefore(now)) {
          writer.addRowBatch(batch);
          batch.reset();

          pathQueue.add(localPath);

          try {
            writer.close();
          } catch (IOException ioexp) {
            log.error(ioexp);
          }

          createWriter(now);
          startingTime = getCurrentStartingDT(now);
        }
      }
    }

    if ((batch.getMaxSize() - batch.size) < 50) {
      synchronized (writerLock) {
        if ((batch.getMaxSize() - batch.size) < 50) {
          writer.addRowBatch(batch);
          batch.reset();
        }
      }
    }
    batch = schema.addRowToBatch(batch, record.split("\\t"));
  }

  private void createWriter(LocalDateTime now) throws IOException {

    localPath = createLocalPath(now);
    writer = OrcFile.createWriter(localPath,
        OrcFile.writerOptions(localConf).setSchema(schema.getTypeDescription()));
    log.debug("local file created " + localPath);
  }

  private Path createLocalPath(LocalDateTime now) {
    String fileName =
        now.atZone(ZoneId.systemDefault()).toEpochSecond() + "-" + UUID.randomUUID() + ".orc";
    String fileFullName = consumerConfig.getCacheFolder() + "/" + fileName;
    return new Path(fileFullName);
  }

  private int getCurrentStartingMinutes(LocalDateTime now) {
    return ((now.getHour() * 60 + now.getMinute()) / consumerConfig.getTimeSliceMinutes())
        * consumerConfig.getTimeSliceMinutes();
  }

  private LocalDateTime getCurrentStartingDT(LocalDateTime now) {
    int startingMinutes = getCurrentStartingMinutes(now);
    return now.minusMinutes(now.getHour() * 60 + now.getMinute() - startingMinutes);
  }
}
